#!/bin/bash

# Redirect output stream to this file.
#PBS -o pbs_out.dat

# Redirect error stream to this file.
#S -e pbs_err.daPBt

# Join error and log files
#PBS -j oe

# Send status information to this email address.
#PBS -M till.sachau@uni-tuebingen.de

# Send an e-mail when the job is done.
##PBS -m e

#PBS -q esd2
#PBS -l nodes=1:ppn=16:esd2
#PBS -l walltime=70:00:00
#PBS -l pmem=12gb

# Change to current working directory (directory where qsub was executed)
DIR=$PBS_O_WORKDIR
DIR=$(echo $DIR | sed s+[/][^/]*[/][^/]*+/beegfs/work+)
cd $DIR

# check
echo $PBS_O_WORKDIR
echo $DIR
echo $PWD

# modules
module load chains/gnu/7.3
module load mpi/openmpi/3.1-gnu-8.5
module load devel/singularity/3.4

mpirun -n 16 singularity exec --bind $DIR:/workspace --pwd /workspace uw2_el8_v3.sif python3 -m mpi4py script.py 1 > job_output.dat 2>&1

# # pbsdsh singularity exec --bind $DIR:/workspace --pwd /workspace uw2_el8_v3.sif python3 -m mpi4py Exp_F_PIC.py 1 > job_output.dat 2>&1
